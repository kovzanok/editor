"use client";
import { SignInButton, UserButton } from "@clerk/clerk-react";
import { Authenticated, AuthLoading, Unauthenticated } from "convex/react";

import { Button } from "@/components/ui/button";
import { Spinner } from "@/components/ui/spinner";

export function Header() {
  return (
    <header>
      <div className="max-w-[1450px] m-auto flex justify-between p-5">
        <h1 className="font-bold text-xl flex-1">Logo</h1>
        <Authenticated>
          <UserButton afterSignOutUrl="/" showName signInUrl="/" />
        </Authenticated>
        <Unauthenticated>
          <SignInButton mode="modal">
            <Button variant={"secondary"}>Sign in</Button>
          </SignInButton>
        </Unauthenticated>
        <AuthLoading>
          <Spinner size={"lg"} />
        </AuthLoading>
      </div>
    </header>
  );
}
