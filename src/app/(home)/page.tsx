"use client";
import { SignInButton } from "@clerk/clerk-react";
import { Authenticated, AuthLoading, Unauthenticated } from "convex/react";
import Link from "next/link";

import { Button } from "@/components/ui/button";
import { Spinner } from "@/components/ui/spinner";
import { useStoreUserEffect } from "@/shared/hooks";

export default function Home() {
  useStoreUserEffect();
  return (
    <div className="flex flex-col justify-center items-center gap-y-5">
      <Unauthenticated>
        <h2 className="font-semibold text-2xl text-center">
          Welcome to Junov Net Book
        </h2>
        <SignInButton mode="modal">
          <Button>Sign in to start</Button>
        </SignInButton>
      </Unauthenticated>
      <AuthLoading>
        <Spinner size={"lg"} />
      </AuthLoading>
      <Authenticated>
        <h2 className="font-semibold text-2xl text-center">
          Welcome to Junov Net Book
        </h2>
        <Button asChild>
          <Link href={"/documents"}>Go to documents</Link>
        </Button>
      </Authenticated>
    </div>
  );
}
