import { Header } from "./_components/widgets/header";

export default function HomeLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <>
      <Header />
      <main className="h-[calc(100vh-200px)] flex flex-col justify-center items-center">
        <div className="max-w-[1450px] m-auto">{children}</div>
      </main>
    </>
  );
}
