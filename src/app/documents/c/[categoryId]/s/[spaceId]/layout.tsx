import React from "react";

import { Id } from "@/../convex/_generated/dataModel";

import { PageList } from "./_components/widgets/page-list";
import { SpaceForm } from "./_components/widgets/space-form";

export default function SpacePage({
  params: { categoryId, spaceId },
  children,
}: {
  params: { spaceId: Id<"space">; categoryId: Id<"categories"> };
  children: React.ReactNode;
}) {
  return (
    <div className="h-full pt-[20px] flex flex-col">
      <div className='relative after:absolute after:w-full after:h-[1px] after:content-[""] after:bg-slate-300 after:top-[calc(100%+20px)]'>
        <SpaceForm _id={spaceId} categoryId={categoryId} />
      </div>
      <div className="flex mt-5 sm:px-5 px-2 flex-1">
        <PageList categoryId={categoryId} spaceId={spaceId} />
        <div className="w-full overflow-x-hidden">{children}</div>
      </div>
    </div>
  );
}
