import { TComment } from "@udecode/plate-comments";
import { Value } from "@udecode/plate-common";
import { Doc } from "convex/_generated/dataModel";
import { useMutation } from "convex/react";
import { useContext } from "react";

import { api } from "@/../convex/_generated/api";
import { CategoryContext, CategoryContextValue } from "@/shared/context";

import { PlateJSEditor } from "../../features/plate-editor";
import { useMappedComments } from "./hooks";

type PageEditorProps = Doc<"page">;

export function PageEditor({
  _id,
  content,
  title,
  description,
}: PageEditorProps) {
  const update = useMutation(api.page.update);
  const createComment = useMutation(api.comments.create);
  const removeComment = useMutation(api.comments.remove);
  const updateComment = useMutation(api.comments.update);
  const { isSharedCategory } = useContext(
    CategoryContext,
  ) as CategoryContextValue;
  const pageContent: Value | undefined = content
    ? JSON.parse(content)
    : undefined;
  const updateContent = (content: string) =>
    update({ content, description, _id, title });
  const addPageComment = ({
    createdAt,
    id,
    parentId,
    userId,
    value,
  }: TComment) =>
    createComment({
      pageId: _id,
      commentId: id,
      createdAt,
      parentId,
      tokenIdentifier: userId,
      value: JSON.stringify(value),
    });
  const comments = useMappedComments(_id);
  return (
    <div>
      <PlateJSEditor
        addPageComment={addPageComment}
        comments={comments}
        pageContent={pageContent}
        readonly={isSharedCategory}
        removeComment={removeComment}
        updateComment={updateComment}
        updateContent={updateContent}
      />
    </div>
  );
}
