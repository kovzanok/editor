import { mixed, object, string } from "yup";

import { Id } from "@/../convex/_generated/dataModel";

export const pageSchema = object({
  _id: mixed<Id<"page">>().required(),
  title: string(),
  description: string(),
});

export const MAX_TITLE_LENGTH = 40;

export const MAX_DESCRIPTION_LENGTH = 60;
