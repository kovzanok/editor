"use client";

import { useQuery } from "convex/react";

import { api } from "@/../convex/_generated/api";
import { Id } from "@/../convex/_generated/dataModel";

import { PageEditor } from "./_components/widgets/page-editor";
import { PageForm } from "./_components/widgets/page-form";
import { PageSkeleton } from "./_components/widgets/page-skeleton";

export default function PageEditorPage({
  params: { categoryId, pageId, spaceId },
}: {
  params: {
    spaceId: Id<"space">;
    categoryId: Id<"categories">;
    pageId: Id<"page">;
  };
}) {
  const page = useQuery(api.page.getById, { _id: pageId, categoryId, spaceId });
  return (
    <div className="mt-5 px-[30px] pb-[30px] flex flex-col gap-y-3">
      {page ? (
        <>
          <PageForm {...page} />
          <PageEditor {...page} />
        </>
      ) : (
        <PageSkeleton />
      )}
    </div>
  );
}
