import { TComment } from "@udecode/plate-comments";
import { Id } from "convex/_generated/dataModel";
import { useQuery } from "convex/react";
import { useMemo } from "react";

import { api } from "@/../convex/_generated/api";

export const useMappedComments = (pageId: Id<"page">) => {
  const comments = useQuery(api.comments.get, { pageId });
  const mappedComments = useMemo(
    () =>
      comments?.reduce<Record<string, TComment>>(
        (acc, { createdAt, commentId, value, parentId, tokenIdentifier }) => {
          acc[commentId] = {
            createdAt,
            id: commentId,
            value: JSON.parse(value),
            parentId,
            userId: tokenIdentifier,
          };
          return acc;
        },
        {},
      ),
    [comments],
  );
  return mappedComments;
};
