"use client";
import { yupResolver } from "@hookform/resolvers/yup";
import { useMutation } from "convex/react";
import { useContext } from "react";
import { SubmitHandler, useForm } from "react-hook-form";

import { api } from "@/../convex/_generated/api";
import { Doc } from "@/../convex/_generated/dataModel";
import { FormWrapper } from "@/components/react-hook-form/form-wrapper";
import { ReactHookFormCategoryInput } from "@/components/react-hook-form/react-hook-form-input";
import { CategoryContext, CategoryContextValue } from "@/shared/context";

import { MAX_DESCRIPTION_LENGTH, MAX_TITLE_LENGTH, pageSchema } from "./const";
import { FormValues } from "./type";

type FormProps = Doc<"page">;

export function PageForm({ title, description, _id }: FormProps) {
  const { isSharedCategory } = useContext(
    CategoryContext,
  ) as CategoryContextValue;
  const methods = useForm({
    resolver: yupResolver(pageSchema),
    defaultValues: { title, description, _id },
  });
  const update = useMutation(api.page.update);
  const handleSubmit: SubmitHandler<FormValues> = data => {
    if (!data.description && !data.title) {
      return;
    }
    update({
      _id: data._id,
      description: data.description,
      title: data.title || title,
    });
  };

  return (
    <div>
      <FormWrapper {...methods} onBlur={handleSubmit}>
        <div className="flex flex-col gap-y-2">
          <ReactHookFormCategoryInput
            className="text-3xl font-bold px-3 py-1"
            disabled={isSharedCategory}
            maxLength={MAX_TITLE_LENGTH}
            name={"title"}
            placeholder="Untitled page"
          />
          <ReactHookFormCategoryInput
            className="text-xl  px-2 py-1"
            disabled={isSharedCategory}
            maxLength={MAX_DESCRIPTION_LENGTH}
            name={"description"}
            placeholder="Page description (optional)"
          />
        </div>
      </FormWrapper>
    </div>
  );
}
