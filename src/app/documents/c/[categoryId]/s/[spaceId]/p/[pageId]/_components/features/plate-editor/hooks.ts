import { useQuery } from "convex/react";
import { useMemo } from "react";

import { api } from "@/../convex/_generated/api";
import { Id } from "@/../convex/_generated/dataModel";

export const useMappedUsers = (_id: Id<"categories">) => {
  const users = useQuery(api.sharedCategories.getAddedUsers, {
    _id,
  });

  const mappedUsers = useMemo(
    () =>
      users?.reduce<
        Record<string, { id: string; name: string; avatarUrl?: string }>
      >((acc, item) => {
        if (item) {
          acc[item.tokenIdentifier] = {
            id: item.tokenIdentifier,
            name: item.fullName || item.nickName,
            avatarUrl: item.avatarUrl,
          };
        }
        return acc;
      }, {}),
    [users],
  );

  return mappedUsers;
};
