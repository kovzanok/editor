"use client";

import { CommentsProvider, TComment } from "@udecode/plate-comments";
import { Plate, PlateEditor, Value } from "@udecode/plate-common";
import { useQuery } from "convex/react";
import { useParams } from "next/navigation";
import { useRef } from "react";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";

import { api } from "@/../convex/_generated/api";
import { Id } from "@/../convex/_generated/dataModel";
import { CommentsPopover } from "@/components/plate-ui/comments-popover";
import { Editor } from "@/components/plate-ui/editor";
import { FixedToolbar } from "@/components/plate-ui/fixed-toolbar";
import { FixedToolbarButtons } from "@/components/plate-ui/fixed-toolbar-buttons";
import { FloatingToolbar } from "@/components/plate-ui/floating-toolbar";
import { FloatingToolbarButtons } from "@/components/plate-ui/floating-toolbar-buttons";
import { TooltipProvider } from "@/components/plate-ui/tooltip";

import { useMappedUsers } from "./hooks";
import { plugins } from "./plugins";

type PlateJSEditorProps = {
  pageContent?: Value;
  updateContent: (content: string) => void;
  addPageComment: (value: TComment) => void;
  removeComment: ({ id }: { id: string }) => void;
  comments: Record<string, TComment> | undefined;
  updateComment: ({ id, value }: { id: string; value: string }) => void;
  readonly?: boolean;
};

export function PlateJSEditor({
  updateContent,
  addPageComment,
  removeComment,
  updateComment,
  pageContent,
  comments,
  readonly,
}: PlateJSEditorProps) {
  const userToken = useQuery(api.users.getTokenIdentifier);
  const params = useParams<{ categoryId: Id<"categories"> }>();
  const users = useMappedUsers(params.categoryId);
  const editorRef = useRef<PlateEditor>(null);
  const timeoutRef = useRef<NodeJS.Timeout | null>(null);
  const handleChange = () => {
    const editor = editorRef.current;
    if (editor) {
      if (timeoutRef.current) {
        clearTimeout(timeoutRef.current);
      }
      timeoutRef.current = setTimeout(() => {
        updateContent(JSON.stringify(editor.children));
      }, 1000);
    }
  };
  if (!userToken) {
    return null;
  }

  return (
    <Plate
      editorRef={editorRef}
      initialValue={pageContent}
      onChange={handleChange}
      plugins={plugins}
      readOnly={readonly}
    >
      <TooltipProvider>
        <DndProvider backend={HTML5Backend}>
          <CommentsProvider
            comments={comments}
            myUserId={userToken.tokenIdentifier}
            onCommentAdd={comment => {
              addPageComment({
                ...comment,
                userId: comment.userId || userToken.tokenIdentifier || "",
              });
            }}
            onCommentDelete={id => removeComment({ id })}
            onCommentUpdate={({ id, value }) =>
              updateComment({ id, value: JSON.stringify(value) })
            }
            users={users}
          >
            <FixedToolbar>
              <FixedToolbarButtons />
            </FixedToolbar>
            <Editor className="mt-4" onContextMenu={e => e.preventDefault()} />
            <FloatingToolbar>
              <FloatingToolbarButtons />
            </FloatingToolbar>
            <CommentsPopover />
          </CommentsProvider>
        </DndProvider>
      </TooltipProvider>
    </Plate>
  );
}
