import { InferType } from "yup";

import { pageSchema } from "./const";

export type FormValues = InferType<typeof pageSchema>;
