import { Skeleton } from "@/components/ui/skeleton";

export function PageSkeleton() {
  return (
    <>
      <div className="flex flex-col gap-y-2">
        <Skeleton className="w-full h-10" />
        <Skeleton className="w-full h-8" />
      </div>
      <Skeleton className="w-full h-48" />
    </>
  );
}
