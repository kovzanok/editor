import { yupResolver } from "@hookform/resolvers/yup";
import { useMutation } from "convex/react";
import { useContext } from "react";
import { SubmitHandler, useForm } from "react-hook-form";

import { api } from "@/../convex/_generated/api";
import { Doc } from "@/../convex/_generated/dataModel";
import { FormWrapper } from "@/components/react-hook-form/form-wrapper";
import { ReactHookFormCategoryInput } from "@/components/react-hook-form/react-hook-form-input";
import { CategoryContext, CategoryContextValue } from "@/shared/context";

import { MAX_TITLE_LENGTH, spaceSchema } from "./const";
import { FormValues } from "./types";

type FormProps = Doc<"space">;

export function Form({ title, _id }: FormProps) {
  const { isSharedCategory } = useContext(
    CategoryContext,
  ) as CategoryContextValue;
  const methods = useForm({
    resolver: yupResolver(spaceSchema),
    defaultValues: { title, _id },
  });
  const update = useMutation(api.space.update);
  const handleSubmit: SubmitHandler<FormValues> = data => {
    if (!data.title) {
      return;
    }
    update({
      _id: data._id,
      title: data.title || title,
    });
  };

  return (
    <FormWrapper {...methods} onBlur={handleSubmit}>
      <ReactHookFormCategoryInput
        className="text-2xl font-semibold px-3 py-1 w-full"
        disabled={isSharedCategory}
        maxLength={MAX_TITLE_LENGTH}
        name={"title"}
        placeholder="Enter title for this collection"
      />
    </FormWrapper>
  );
}
