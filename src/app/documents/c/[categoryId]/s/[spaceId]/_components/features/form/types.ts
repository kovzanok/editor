import { InferType } from "yup";

import { spaceSchema } from "./const";

export type FormValues = InferType<typeof spaceSchema>;
