"use client";
import { useQuery } from "convex/react";
import { CirclePlus, PanelRightOpen } from "lucide-react";
import { useContext, useEffect, useState } from "react";
import { useMediaQuery } from "usehooks-ts";

import { api } from "@/../convex/_generated/api";
import { Id } from "@/../convex/_generated/dataModel";
import { Button } from "@/components/ui/button";
import { CategoryContext, CategoryContextValue } from "@/shared/context";
import { useEntityCreate } from "@/shared/hooks";
import { cn } from "@/shared/lib";

import { PageItem } from "./components/page-item";
import { PageListSkeleton } from "./components/page-list-skeleton";

type PageListProps = { spaceId: Id<"space">; categoryId: Id<"categories"> };

export function PageList({ spaceId, categoryId }: PageListProps) {
  const pages = useQuery(api.page.get, { spaceId });
  const isTablet = useMediaQuery("(max-width: 768px)");
  const [open, setOpen] = useState(!isTablet);
  useEffect(() => setOpen(!isTablet), [isTablet]);
  const { isSharedCategory } = useContext(
    CategoryContext,
  ) as CategoryContextValue;
  const { handleCreate, isLoading } = useEntityCreate(
    api.page.create,
    [{ spaceId, categoryId }],
    {
      success: "Page created",
      loading: "Creating page...",
      error: "Failed to create page",
    },
  );

  return (
    <div className="relative">
      <aside
        className={cn(
          'relative h-full w-0 after:absolute transition-all overflow-y-auto after:top-0 after:right-0 after:h-full after:content-[""] after:w-[2px] after:bg-slate-300',
          open && "w-[250px]",
        )}
      >
        {pages ? (
          <ul className="flex flex-col py-4 gap-y-2">
            {pages.map(
              p =>
                p && (
                  <PageItem
                    editable={!isSharedCategory}
                    key={p._id}
                    onClick={() => {
                      if (isTablet) {
                        setOpen(false);
                      }
                    }}
                    {...p}
                    isLast={pages.length === 1}
                  />
                ),
            )}
            {!isSharedCategory && (
              <li className="w-full pr-4">
                <button
                  className="flex gap-x-2 hover:bg-slate-200 disabled:hover:bg-transparent disabled:hover:text-slate-400 p-2 rounded w-full text-slate-400 hover:text-slate-600 transition-colors"
                  disabled={isLoading}
                  onClick={handleCreate}
                >
                  <CirclePlus />
                  <p>Add new page...</p>
                </button>
              </li>
            )}
          </ul>
        ) : (
          <PageListSkeleton />
        )}
      </aside>
      <div className="group absolute top-0 right-0 translate-x-full w-7 h-full pt-2 pl-2">
        <Button
          className="group-hover:opacity-100 group-hover:visible md:invisible md:opacity-0 transition-opacity p-1 h-auto w-auto"
          onClick={() => setOpen(v => !v)}
          variant={"outline"}
        >
          <PanelRightOpen
            className={cn("text-slate-400", !open && "rotate-180")}
            size={20}
          />
        </Button>
      </div>
    </div>
  );
}
