import { Skeleton } from "@/components/ui/skeleton";

export function PageListSkeleton() {
  return (
    <ul className="flex flex-col py-4 gap-y-2">
      {new Array(4).fill(0).map((_, idx) => (
        <Skeleton className="h-10 w-full rounded-r-none" key={idx} />
      ))}
    </ul>
  );
}
