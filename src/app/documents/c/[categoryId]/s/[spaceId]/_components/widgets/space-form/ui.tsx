"use client";
import { useQuery } from "convex/react";

import { api } from "@/../convex/_generated/api";
import { Id } from "@/../convex/_generated/dataModel";

import { Form } from "../../features/form";
import { FormSkeleton } from "./components/skeletion";

type SpaceFormProps = { _id: Id<"space">; categoryId: Id<"categories"> };

export function SpaceForm({ _id, categoryId }: SpaceFormProps) {
  const data = useQuery(api.space.getById, { _id, categoryId });

  return (
    <div className="sm:px-20 px-10">
      {data ? <Form {...data} /> : <FormSkeleton />}
    </div>
  );
}
