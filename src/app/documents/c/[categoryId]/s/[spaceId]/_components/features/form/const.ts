import { mixed, object, string } from "yup";

import { Id } from "@/../convex/_generated/dataModel";

export const spaceSchema = object({
  _id: mixed<Id<"space">>().required(),
  title: string(),
});

export const MAX_TITLE_LENGTH = 40;
