import { Trash } from "lucide-react";
import Link from "next/link";
import { useParams } from "next/navigation";
import React, { MouseEventHandler, useState } from "react";

import { api } from "@/../convex/_generated/api";
import { Doc, Id } from "@/../convex/_generated/dataModel";
import { Button } from "@/components/ui/button";
import { ControlsDropdown } from "@/components/ui/controls-dropdown";
import { useRemoveById } from "@/shared/hooks";
import { cn } from "@/shared/lib";
type PageItemProps = {
  spaceId: Id<"space">;
  categoryId: Id<"categories">;
  isLast: boolean;
  editable: boolean;
  onClick: MouseEventHandler<HTMLAnchorElement>;
} & Doc<"page">;

export function PageItem({
  categoryId,
  spaceId,
  title,
  _id,
  isLast,
  editable,
  onClick,
}: PageItemProps) {
  const { pageId } = useParams();
  const [open, setOpen] = useState(false);
  const handlePageRemove = useRemoveById(api.page.remove, [{ _id, spaceId }], {
    success: "Page removed",
    error: "Failed to remove page",
    loading: "Removing page...",
  });
  return (
    <li
      className={cn(
        "group flex w-full rounded-l hover:bg-slate-200 p-2",
        pageId === _id && "bg-slate-200",
      )}
    >
      <Link
        className="w-full inline-block"
        href={`/documents/c/${categoryId}/s/${spaceId}/p/${_id}`}
        onClick={onClick}
      >
        {title}
      </Link>
      {!isLast && editable && (
        <ControlsDropdown onOpenChange={setOpen} open={open}>
          <Button
            className="flex items-center justify-center gap-x-3 w-full"
            onClick={handlePageRemove}
            variant={"outline"}
          >
            <Trash size={18} />
            Delete
          </Button>
        </ControlsDropdown>
      )}
    </li>
  );
}
