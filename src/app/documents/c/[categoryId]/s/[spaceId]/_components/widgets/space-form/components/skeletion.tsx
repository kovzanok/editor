import { Skeleton } from "@/components/ui/skeleton";

export function FormSkeleton() {
  return <Skeleton className="w-full h-10" />;
}
