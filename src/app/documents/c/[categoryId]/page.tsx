import { Id } from "@/../convex/_generated/dataModel";

import { CategoryForm } from "./_components/widgets/category-form";
import { CategorySpaceList } from "./_components/widgets/category-space-list";

export default function CategoryPage({
  params: { categoryId },
}: {
  params: { categoryId: Id<"categories"> };
}) {
  return (
    <div className="flex flex-col gap-y-5 py-[40px]">
      <div className="lg:px-[150px] sm:px-[50px] px-3">
        <CategoryForm categoryId={categoryId} />
      </div>
      <div className="border-t-2 border-slate-300 pt-5">
        <div className="lg:px-[150px] sm:px-[50px] px-3">
          <CategorySpaceList categoryId={categoryId} />
        </div>
      </div>
    </div>
  );
}
