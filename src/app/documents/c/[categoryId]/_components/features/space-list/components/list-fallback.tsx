import { Component } from "lucide-react";
import { MouseEventHandler, useContext } from "react";

import { Button } from "@/components/ui/button";
import { CategoryContext, CategoryContextValue } from "@/shared/context";

type ListFallbackProps = {
  isSpaceCreating: boolean;
  handleCreate: MouseEventHandler<HTMLButtonElement>;
};

export function ListFallback({
  handleCreate,
  isSpaceCreating,
}: ListFallbackProps) {
  const { isSharedCategory } = useContext(
    CategoryContext,
  ) as CategoryContextValue;
  return (
    <div className="w-full rounded shadow border-gray-200 py-6 flex flex-col gap-y-2 items-center">
      <p>No content yet</p>
      <p className="text-sm text-slate-400">
        {isSharedCategory
          ? "This category is empty, wait until category creator fix it!"
          : "This category is empty, let's fix it!"}
      </p>
      <Button
        className="flex gap-x-2 w-fit justify-self-end mt-4"
        disabled={isSpaceCreating || isSharedCategory}
        onClick={handleCreate}
      >
        <Component size={14} />
        New space
      </Button>
    </div>
  );
}
