import { Component, EllipsisVertical, Trash } from "lucide-react";
import Link from "next/link";
import { useContext } from "react";

import { api } from "@/../convex/_generated/api";
import { Doc, Id } from "@/../convex/_generated/dataModel";
import { Button } from "@/components/ui/button";
import { ControlsDropdown } from "@/components/ui/controls-dropdown";
import { CategoryContext, CategoryContextValue } from "@/shared/context";
import { useRemoveById } from "@/shared/hooks";

type SpaceItemProps = Doc<"space"> & {
  categoryId: Id<"categories">;
};

export function SpaceItem({ categoryId, _id, title }: SpaceItemProps) {
  const handleSpaceDelete = useRemoveById(
    api.space.remove,
    [{ _id, categoryId }],
    {
      success: "Space deleted",
      error: "Failed to delete space",
      loading: "Deleting space",
    },
  );
  const { isSharedCategory } = useContext(
    CategoryContext,
  ) as CategoryContextValue;
  return (
    <Link
      className="relative block border border-slate-400 hover:border-slate-500 transition-colors rounded"
      href={`/documents/c/${categoryId}/s/${_id}`}
      title={title}
    >
      <div className="bg-slate-300 rounded-t h-[30px] border-slate-400 border-b"></div>
      <div className="absolute top-[15px] left-2 flex justify-center items-center bg-slate-200 border-slate-400 border rounded p-1">
        <Component size={16} />
      </div>
      <div className="relative px-3 py-5">
        <p className="text-ellipsis overflow-hidden whitespace-nowrap">
          {title}
        </p>
        <ControlsDropdown
          disabled={isSharedCategory}
          triggerButton={
            <Button
              className="absolute top-0 right-0  group bg-transparent hover:bg-transparent border-none p-2 h-auto"
              variant={"outline"}
            >
              <EllipsisVertical size={14} />
            </Button>
          }
        >
          <Button
            className="flex items-center justify-center gap-x-3 w-full"
            onClick={handleSpaceDelete}
            variant={"outline"}
          >
            <Trash size={18} />
            Delete
          </Button>
        </ControlsDropdown>
      </div>
    </Link>
  );
}
