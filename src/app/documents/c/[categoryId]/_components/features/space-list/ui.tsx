import { Component } from "lucide-react";
import { MouseEventHandler, useContext } from "react";

import { Doc } from "@/../convex/_generated/dataModel";
import { Button } from "@/components/ui/button";
import { CategoryContext, CategoryContextValue } from "@/shared/context";

import { ListFallback } from "./components/list-fallback";
import { SpaceItem } from "./components/space-item";

type SpaceListProps = {
  space: (Doc<"space"> | null)[];
  handleCreate: MouseEventHandler<HTMLButtonElement>;
  isSpaceCreating: boolean;
};

export function SpaceList({
  space,
  handleCreate,
  isSpaceCreating,
}: SpaceListProps) {
  const { isSharedCategory } = useContext(
    CategoryContext,
  ) as CategoryContextValue;
  return (
    <div className="grid gap-y-4">
      <Button
        className="flex gap-x-2 w-fit justify-self-end"
        disabled={isSpaceCreating || isSharedCategory}
        onClick={e => {
          if (isSharedCategory) {
            return;
          }
          handleCreate(e);
        }}
      >
        <Component size={14} />
        New space
      </Button>
      {space.length === 0 ? (
        <ListFallback
          handleCreate={e => {
            if (isSharedCategory) {
              return;
            }
            handleCreate(e);
          }}
          isSpaceCreating={isSpaceCreating}
        />
      ) : (
        <ul className="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 grid-cols-1 gap-4 lg:flex-nowrap flex-wrap">
          {space.map(s => s && <SpaceItem {...s} key={s?._id} />)}
        </ul>
      )}
    </div>
  );
}
