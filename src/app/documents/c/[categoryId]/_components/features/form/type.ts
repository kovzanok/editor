import { InferType } from "yup";

import { categorySchema } from "./const";

export type FormValues = InferType<typeof categorySchema>;
