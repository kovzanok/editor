import { yupResolver } from "@hookform/resolvers/yup";
import { useContext } from "react";
import { SubmitHandler, useForm } from "react-hook-form";

import { Doc } from "@/../convex/_generated/dataModel";
import { FormWrapper } from "@/components/react-hook-form/form-wrapper";
import { ReactHookFormCategoryInput } from "@/components/react-hook-form/react-hook-form-input";
import { CategoryContext, CategoryContextValue } from "@/shared/context";

import {
  categorySchema,
  MAX_DESCRIPTION_LENGTH,
  MAX_TITLE_LENGTH,
} from "./const";
import { FormValues } from "./type";

type FormProps = Doc<"categories"> & {
  submitHandler: (data: FormValues) => void;
};

export function Form({ title, description, _id, submitHandler }: FormProps) {
  const { isSharedCategory } = useContext(
    CategoryContext,
  ) as CategoryContextValue;
  const methods = useForm({
    resolver: yupResolver(categorySchema),
    defaultValues: { title, description, _id },
    mode: "onChange",
  });

  const handleSubmit: SubmitHandler<FormValues> = data => {
    if (!data.description && !data.title) {
      return;
    }
    submitHandler({
      _id: data._id,
      description: data.description || description,
      title: data.title || title,
    });
  };

  return (
    <FormWrapper {...methods} onBlur={handleSubmit}>
      <div className="flex flex-col gap-y-2">
        <ReactHookFormCategoryInput
          className="text-3xl font-bold px-3 py-1"
          disabled={isSharedCategory}
          maxLength={MAX_TITLE_LENGTH}
          name={"title"}
          placeholder="Enter title for this collection"
        />
        <ReactHookFormCategoryInput
          className="text-xl  px-2 py-1"
          disabled={isSharedCategory}
          maxLength={MAX_DESCRIPTION_LENGTH}
          name={"description"}
          placeholder="Description for this collection"
        />
      </div>
    </FormWrapper>
  );
}
