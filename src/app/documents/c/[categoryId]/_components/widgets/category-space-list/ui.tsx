"use client";
import { useQuery } from "convex/react";

import { api } from "@/../convex/_generated/api";
import { Id } from "@/../convex/_generated/dataModel";
import { useEntityCreate } from "@/shared/hooks";

import { SpaceList } from "../../features/space-list";
import { SpaceListSkeleton } from "./components/space-list-skeleton";

type CategorySpaceListProps = { categoryId: Id<"categories"> };

export function CategorySpaceList({ categoryId }: CategorySpaceListProps) {
  const space = useQuery(api.space.get, { categoryId });
  const { handleCreate, isLoading } = useEntityCreate(
    api.space.create,
    [{ categoryId }],
    {
      success: "Space created",
      loading: "Creating space...",
      error: "Failed to create space",
    },
  );

  if (space === undefined) {
    return <SpaceListSkeleton />;
  }
  return (
    <SpaceList
      handleCreate={handleCreate}
      isSpaceCreating={isLoading}
      space={space}
    />
  );
}
