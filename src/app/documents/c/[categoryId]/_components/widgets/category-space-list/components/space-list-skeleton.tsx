import { Skeleton } from "@/components/ui/skeleton";

export function SpaceListSkeleton() {
  return (
    <div className="grid gap-y-4">
      <Skeleton className="justify-self-end w-32 h-10" />
      <ul className="grid grid-cols-4 gap-4">
        {new Array(8).fill(0).map((_, idx) => (
          <Skeleton className="h-[95px] w-full" key={idx} />
        ))}
      </ul>
    </div>
  );
}
