"use client";
import { useMutation, useQuery } from "convex/react";

import { api } from "@/../convex/_generated/api";
import { Id } from "@/../convex/_generated/dataModel";

import { Form } from "../../features/form";
import { FormSkeleton } from "./components/skeleton";

type CategoryFormProps = { categoryId: Id<"categories"> };

export function CategoryForm({ categoryId }: CategoryFormProps) {
  const categoryData = useQuery(api.categories.getById, { _id: categoryId });
  const update = useMutation(api.categories.update);
  return categoryData ? (
    <Form submitHandler={update} {...categoryData} />
  ) : (
    <FormSkeleton />
  );
}
