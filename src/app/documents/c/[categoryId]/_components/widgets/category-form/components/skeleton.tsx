import React from "react";

import { Skeleton } from "@/components/ui/skeleton";

export function FormSkeleton() {
  return (
    <div className="flex flex-col gap-y-2">
      <Skeleton className="h-11 w-full" />
      <Skeleton className="h-[36px] w-full" />
    </div>
  );
}
