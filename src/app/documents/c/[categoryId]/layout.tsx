"use client";

import { Id } from "@/../convex/_generated/dataModel";
import { useCategoryContext } from "@/shared/context";

export default function CategoryLayout({
  children,
  params: { categoryId },
}: Readonly<{
  children: React.ReactNode;
  params: { categoryId: Id<"categories"> };
}>) {
  useCategoryContext(categoryId);
  return <div className="flex flex-col h-full">{children}</div>;
}
