import { useQuery } from "convex/react";

import { api } from "@/../convex/_generated/api";

import { Categories } from "../../../features/categories";

type MyCategoriesProps = {
  onItemClick?: () => void;
};

export function MyCategories({ onItemClick }: MyCategoriesProps) {
  const myCategories = useQuery(api.categories.getUserCategories);
  return <Categories categories={myCategories} onItemClick={onItemClick} />;
}
