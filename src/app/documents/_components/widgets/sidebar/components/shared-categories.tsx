import { useQuery } from "convex/react";

import { api } from "@/../convex/_generated/api";

import { Categories } from "../../../features/categories";

type SharedCategoriesProps = {
  onItemClick?: () => void;
};

export function SharedCategories({ onItemClick }: SharedCategoriesProps) {
  const sharedCategories = useQuery(api.sharedCategories.getSharedCategories);
  return (
    <Categories
      categories={sharedCategories}
      isShared
      onItemClick={onItemClick}
    />
  );
}
