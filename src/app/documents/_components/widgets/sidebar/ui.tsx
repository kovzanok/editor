import { ChevronsLeft, ChevronsRight } from "lucide-react";
import { useEffect, useState } from "react";
import { useMediaQuery } from "usehooks-ts";

import { Button } from "@/components/ui/button";
import { cn } from "@/shared/lib";

import { MyCategories } from "./components/my-categories";
import { SharedCategories } from "./components/shared-categories";
import { UserItem } from "./components/user-item";

export function Sidebar() {
  const isTablet = useMediaQuery("(max-width: 768px)");
  const [open, setOpen] = useState(!isTablet);
  useEffect(() => setOpen(!isTablet), [isTablet]);

  const handleItemClick = () => {
    if (isTablet) {
      setOpen(false);
    }
  };

  return (
    <div className={cn("relative z-50", isTablet && open && "w-full")}>
      <aside
        className={cn(
          "relative group/sidebar h-full overflow-y-auto transition-all bg-gray-100 w-0",
          open && isTablet && "absolute top-0 left-0 w-full h-full",
          open && !isTablet && "w-[250px]",
        )}
      >
        <Button
          className={cn(
            "w-8 h-8 bg-transparent p-0 absolute right-2 top-2 opacity-0 group-hover/sidebar:opacity-100 transition-all",
            isTablet && "opacity-100",
          )}
          onClick={() => setOpen(false)}
          variant={"outline"}
        >
          <ChevronsLeft className="h-8 w-8" />
        </Button>
        <UserItem />
        <MyCategories onItemClick={handleItemClick} />
        <SharedCategories onItemClick={handleItemClick} />
      </aside>
      {!open && (
        <Button
          className="fixed top-2 left-2 p-0 w-8 h-8"
          onClick={() => setOpen(true)}
          variant={"outline"}
        >
          <ChevronsRight className="h-6 w-6" />
        </Button>
      )}
    </div>
  );
}
