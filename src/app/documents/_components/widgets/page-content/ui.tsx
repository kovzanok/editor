import { CreateCategoryButton } from "./components/create-category-button";
import { UserGreeting } from "./components/user-greeting";

export function PageContent() {
  return (
    <div className="h-full flex flex-col justify-center items-center gap-y-4">
      <UserGreeting />
      <CreateCategoryButton />
    </div>
  );
}
