"use client";
import { useUser } from "@clerk/nextjs";

export function UserGreeting() {
  const { user } = useUser();
  return <div> Welcome, {user?.firstName || user?.username}</div>;
}
