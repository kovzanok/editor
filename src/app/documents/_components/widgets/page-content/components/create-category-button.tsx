"use client";
import { PlusCircleIcon } from "lucide-react";

import { Button } from "@/components/ui/button";
import { useCategoryCreate } from "@/shared/hooks";

export function CreateCategoryButton() {
  const { handleCreate, isLoading } = useCategoryCreate();
  return (
    <Button
      className="flex gap-x-2"
      disabled={isLoading}
      onClick={handleCreate}
    >
      Create collection
      <PlusCircleIcon />
    </Button>
  );
}
