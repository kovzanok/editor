import { useMutation, useQuery } from "convex/react";
import { UserPlus } from "lucide-react";
import React from "react";

import { api } from "@/../convex/_generated/api";
import { Id } from "@/../convex/_generated/dataModel";
import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { ScrollArea } from "@/components/ui/scroll-area";

import { UserAccessItem } from "./components/user-access-item";
import { UserAddForm } from "./components/user-add-form";

type AddUserModal = { categoryId: Id<"categories"> };

export function AddUser({ categoryId }: AddUserModal) {
  const removeUser = useMutation(api.sharedCategories.removeUser);
  const addedUsers = useQuery(api.sharedCategories.getAddedUsers, {
    _id: categoryId,
  });
  const userTokenId = useQuery(api.users.getTokenIdentifier);
  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button
          className="flex items-center justify-center gap-x-3 w-full"
          data-trigger
          variant={"outline"}
        >
          <UserPlus size={18} />
          Add users
        </Button>
      </DialogTrigger>
      <DialogContent className="md:h-auto h-full">
        <DialogHeader>
          <DialogTitle>Add users</DialogTitle>
          <DialogDescription>Invite people to your category</DialogDescription>
        </DialogHeader>
        <div>
          <div className="flex flex-col gap-y-5">
            <UserAddForm addedUsers={addedUsers} categoryId={categoryId} />
            <div className="w-full rounded border">
              <div className="w-full border-b">
                <div className="p-2">Users with access</div>
              </div>
              <div className="p-4">
                <ScrollArea className="w-full h-[200px]">
                  <ul className="flex flex-col gap-y-4">
                    {addedUsers &&
                      addedUsers.map(
                        u =>
                          u && (
                            <UserAccessItem
                              active
                              email={u.email}
                              fullName={u.fullName}
                              handleRemove={() =>
                                removeUser({
                                  categoryId,
                                  userToken: u.tokenIdentifier,
                                })
                              }
                              imageUrl={u.avatarUrl}
                              isCurrentUser={
                                u.tokenIdentifier ===
                                userTokenId?.tokenIdentifier
                              }
                              key={u._id}
                              nickName={u.nickName}
                            />
                          ),
                      )}
                  </ul>
                </ScrollArea>
              </div>
            </div>
          </div>
        </div>
      </DialogContent>
    </Dialog>
  );
}
