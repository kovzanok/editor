import { useMutation } from "convex/react";
import { useState } from "react";

import { api } from "@/../convex/_generated/api";
import { Doc, Id } from "@/../convex/_generated/dataModel";
import { Button } from "@/components/ui/button";

import { UserChipInput } from "./user-chip-input";
type UserAddFormProps = {
  categoryId: Id<"categories">;
  addedUsers: (Doc<"users"> | null)[] | undefined;
};

export function UserAddForm({ categoryId, addedUsers }: UserAddFormProps) {
  const [usersToAdd, setUsersToAdd] = useState<string[]>([]);
  const addUsers = useMutation(api.sharedCategories.addUsers);
  const [isDisabled, setIsDisabled] = useState(false);
  const handleSubmit = () => {
    setIsDisabled(true);
    addUsers({ categoryId, tokensArr: usersToAdd }).then(() => {
      setUsersToAdd([]);
      setIsDisabled(false);
    });
  };

  return (
    <div className="flex gap-x-3">
      <UserChipInput
        addedUsers={addedUsers}
        setUsersToAdd={setUsersToAdd}
        usersToAdd={usersToAdd}
      />
      <Button
        disabled={usersToAdd.length === 0 || isDisabled}
        onClick={handleSubmit}
      >
        Add
      </Button>
    </div>
  );
}
