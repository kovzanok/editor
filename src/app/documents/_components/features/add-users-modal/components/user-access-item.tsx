import { Trash } from "lucide-react";
import React from "react";

import { Avatar, AvatarImage } from "@/components/ui/avatar";
import { Button } from "@/components/ui/button";

type UserAccessItemProps = {
  imageUrl?: string;
  fullName?: string | null;
  nickName?: string | null;
  email?: string;
  isCurrentUser?: boolean;
  active?: boolean;
  handleRemove?: () => void;
};

export function UserAccessItem({
  email,
  fullName,
  imageUrl,
  nickName,
  isCurrentUser,
  active,
  handleRemove,
}: UserAccessItemProps) {
  return (
    <li className="flex items-center gap-x-5">
      <Avatar className="h-8 w-8">
        <AvatarImage src={imageUrl} />
      </Avatar>
      <div>
        <div>
          {fullName || nickName} {isCurrentUser && "(you)"}
        </div>
        <div className="text-slate-400 text-sm font-semibold">{email}</div>
      </div>
      {!isCurrentUser && active && (
        <Button className="ml-auto" onClick={handleRemove} variant={"outline"}>
          <Trash size={20} />
        </Button>
      )}
    </li>
  );
}
