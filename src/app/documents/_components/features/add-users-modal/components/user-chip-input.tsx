import { useUser } from "@clerk/clerk-react";
import { useQuery } from "convex/react";
import { X } from "lucide-react";
import { Dispatch, SetStateAction, useEffect, useRef, useState } from "react";

import { api } from "@/../convex/_generated/api";
import { Doc } from "@/../convex/_generated/dataModel";
import { Button } from "@/components/ui/button";
import { ScrollArea } from "@/components/ui/scroll-area";
import { cn } from "@/shared/lib";

import { useUnaddedUsers } from "../hooks";
import { UserAccessItem } from "./user-access-item";

type ChipInputProps = {
  usersToAdd: string[];
  setUsersToAdd: Dispatch<SetStateAction<string[]>>;
  addedUsers: (Doc<"users"> | null)[] | undefined;
};

export function UserChipInput({
  setUsersToAdd,
  usersToAdd,
  addedUsers,
}: ChipInputProps) {
  const { user } = useUser();
  const divRef = useRef<HTMLDivElement>(null);
  const inputRef = useRef<HTMLInputElement>(null);
  const users = useQuery(api.users.get);
  const [focus, setFocus] = useState(false);
  const [search, setSearch] = useState("");
  const filteredUsers = useUnaddedUsers(
    users || [],
    addedUsers,
    search,
    user?.primaryEmailAddress?.emailAddress,
  );
  useEffect(() => {
    const handleClick = (e: MouseEvent) => {
      if (!divRef.current || !(e.target instanceof HTMLElement)) {
        return;
      }

      if (
        !divRef.current.contains(e.target) &&
        !e.target.dataset.trigger &&
        !e.target.closest("[data-trigger]")
      ) {
        setFocus(false);
      }
    };
    document.addEventListener("click", handleClick);
    return () => document.removeEventListener("click", handleClick);
  }, [setFocus]);

  const handleWrapperClick = () => {
    inputRef.current?.focus();
  };
  console.log(focus);
  return (
    <div className="relative w-full" onClick={handleWrapperClick} ref={divRef}>
      <div className="w-full h-full flex gap-x-2 flex-wrap rounded p-2 border">
        {users &&
          users
            .filter(u => usersToAdd.includes(u.tokenIdentifier))
            .map(u => (
              <Button
                className="flex gap-x-2 items-center px-2 py-1 h-auto"
                data-trigger
                key={u._id}
                onClick={() =>
                  setUsersToAdd(s => s.filter(t => t !== u.tokenIdentifier))
                }
                variant={"outline"}
              >
                {u.fullName || u.nickName}
                <X size={12} />
              </Button>
            ))}
        <input
          className="flex-1 focus:outline-none"
          onChange={e => setSearch(e.target.value)}
          onFocus={() => setFocus(true)}
          ref={inputRef}
          value={search}
        />
      </div>
      {focus && (
        <div className="absolute top-full translate-y-1 left-0 w-full z-50">
          <ScrollArea className=" h-[150px] ">
            <ul className="bg-white border flex flex-col gap-y-3 p-4">
              {filteredUsers.map(u => (
                <li
                  className={cn(
                    "w-full",
                    usersToAdd.includes(u.tokenIdentifier) && "bg-slate-200",
                  )}
                  key={u._id}
                >
                  <button
                    className="p-1 rounded flex items-center gap-x-2 w-full"
                    onClick={() =>
                      setUsersToAdd(s => [...s, u.tokenIdentifier])
                    }
                  >
                    <UserAccessItem
                      email={u.email}
                      fullName={u.fullName}
                      imageUrl={u.avatarUrl}
                      nickName={u.nickName}
                    />
                  </button>
                </li>
              ))}
              {filteredUsers.length === 0 && <div>No users found</div>}
            </ul>
          </ScrollArea>
        </div>
      )}
    </div>
  );
}
