import { useMemo } from "react";

import { Doc } from "@/../convex/_generated/dataModel";

export const useUnaddedUsers = (
  users: Doc<"users">[],
  addedUsers: (Doc<"users"> | null)[] | undefined,
  search: string,
  currentUserEmail?: string,
) => {
  const filteredUsers = useMemo(
    () =>
      users.filter(
        ({ email, fullName, nickName, _id }) =>
          (email.includes(search) ||
            fullName?.includes(search) ||
            nickName.includes(search)) &&
          !addedUsers?.find(u => u?._id === _id) &&
          email !== currentUserEmail,
      ) || [],
    [users, search, addedUsers],
  );
  return filteredUsers;
};
