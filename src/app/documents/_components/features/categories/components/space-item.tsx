import { Component, Trash } from "lucide-react";
import Link from "next/link";
import { useParams } from "next/navigation";
import { useState } from "react";

import { api } from "@/../convex/_generated/api";
import { Doc, Id } from "@/../convex/_generated/dataModel";
import { Button } from "@/components/ui/button";
import { ControlsDropdown } from "@/components/ui/controls-dropdown";
import { useRemoveById } from "@/shared/hooks";
import { cn } from "@/shared/lib";

type SpaceItemProps = Doc<"space"> & {
  categoryId: Id<"categories">;
  onItemClick?: () => void;
  isMyCategory: boolean;
};

export function SpaceItem({
  title,
  categoryId,
  _id,
  isMyCategory,
  onItemClick,
}: SpaceItemProps) {
  const [open, setOpen] = useState(false);
  const { spaceId } = useParams();
  const handleSpaceRemove = useRemoveById(
    api.space.remove,
    [{ categoryId, _id }],
    {
      loading: "Removing space...",
      error: "Failed to remove space",
      success: "Space deleted",
    },
  );

  return (
    <li>
      <Link
        className={cn(
          "flex group items-center hover:bg-gray-300 transition cursor-pointer py-1 pl-6 pr-3 gap-x-2",
          spaceId === _id && "bg-gray-300",
        )}
        href={`/documents/c/${categoryId}/s/${_id}`}
        onClick={onItemClick}
        title={title}
      >
        <Component className="min-w-[15px]" size={15} />
        <p className="max-w-full text-sm text-ellipsis overflow-hidden whitespace-nowrap">
          {title}
        </p>
        {isMyCategory && (
          <div className="flex flex-1 justify-end">
            <ControlsDropdown onOpenChange={setOpen} open={open}>
              <Button
                className="flex items-center justify-center gap-x-3 w-full"
                onClick={handleSpaceRemove}
                variant={"outline"}
              >
                <Trash size={18} />
                Delete
              </Button>
            </ControlsDropdown>
          </div>
        )}
      </Link>
    </li>
  );
}
