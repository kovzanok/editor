import { Skeleton } from "@/components/ui/skeleton";

export function CategoriesListSkeleton() {
  return (
    <div className="px-3 flex flex-col gap-y-2">
      <Skeleton className="w-full h-[32px] bg-slate-300" />
      <Skeleton className="w-full h-[32px] bg-slate-300" />
      <Skeleton className="w-full h-[32px] bg-slate-300" />
    </div>
  );
}
