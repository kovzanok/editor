import { useQuery } from "convex/react";
import { ChevronRight, LibraryBig, PlusCircle, Trash } from "lucide-react";
import Link from "next/link";
import { useParams } from "next/navigation";
import React, { MouseEventHandler, useState } from "react";

import { api } from "@/../convex/_generated/api";
import { Doc } from "@/../convex/_generated/dataModel";
import { Button } from "@/components/ui/button";
import { ControlsDropdown } from "@/components/ui/controls-dropdown";
import { useEntityCreate, useRemoveById } from "@/shared/hooks";
import { cn } from "@/shared/lib";

import { AddUser } from "../../add-users-modal";
import { SpaceItem } from "./space-item";

type CategoryItemProps = Doc<"categories"> & {
  isMyCategory: boolean;
  onItemClick?: () => void;
};

export function CategoryItem({
  title,
  _id,
  isMyCategory,
  onItemClick,
}: CategoryItemProps) {
  const { categoryId, spaceId } = useParams();
  const [open, setOpen] = useState(false);
  const [disabled, setDisabled] = useState(false);
  const [collapse, setCollapse] = useState(categoryId === _id);
  const categorySpace = useQuery(api.space.get, { categoryId: _id });

  const handleCollapse: MouseEventHandler<HTMLButtonElement> = e => {
    e.preventDefault();
    e.stopPropagation();
    setCollapse(v => !v);
  };

  const handleCategoryDelete = useRemoveById(
    api.categories.remove,
    [{ _id }],
    {
      loading: "Deleting category...",
      error: "Failed to delete category",
      success: "Category deleted",
    },
    () => setDisabled(true),
  );

  const { handleCreate, isLoading } = useEntityCreate(
    api.space.create,
    [{ categoryId: _id }],
    {},
  );

  return (
    <li className="flex flex-col">
      <Link
        className={cn(
          "flex group items-center hover:bg-gray-300 transition cursor-pointer py-1 px-3 gap-x-2",
          !spaceId && categoryId === _id && "bg-gray-300",
        )}
        href={`/documents/c/${_id}`}
        onClick={onItemClick}
        title={title}
      >
        <LibraryBig className="min-w-[20px]" size={20} />
        <p className="max-w-full text-ellipsis overflow-hidden whitespace-nowrap">
          {title}
        </p>
        <button onClick={handleCollapse}>
          <ChevronRight
            className={cn("min-w-[25px]", collapse && "rotate-90")}
          />
        </button>
        {isMyCategory && (
          <div className="flex flex-1 justify-end">
            <ControlsDropdown
              disabled={disabled || isLoading}
              onOpenChange={setOpen}
              open={open}
            >
              <Button
                className="flex items-center justify-center gap-x-3 w-full"
                onClick={e => {
                  handleCreate(e);
                  setOpen(false);
                }}
                variant={"outline"}
              >
                <PlusCircle size={18} />
                Add space
              </Button>
              <AddUser categoryId={_id} />
              <Button
                className="flex items-center justify-center gap-x-3 w-full"
                onClick={handleCategoryDelete}
                variant={"outline"}
              >
                <Trash size={18} />
                Delete
              </Button>
            </ControlsDropdown>
          </div>
        )}
      </Link>
      {collapse && categorySpace && categorySpace.length !== 0 && (
        <ul className="flex flex-col gap-y-1 mt-1">
          {categorySpace.map(
            s =>
              s && (
                <SpaceItem
                  isMyCategory={isMyCategory}
                  key={s._id}
                  onItemClick={onItemClick}
                  {...s}
                />
              ),
          )}
        </ul>
      )}
    </li>
  );
}
