import React from "react";

import { MyCategoriesHeader } from "./my-categories-header";

type CategoriesListHeaderProps = {
  isShared?: boolean;
};

export function CategoriesListHeader({ isShared }: CategoriesListHeaderProps) {
  return isShared ? (
    <div className="flex items-center justify-between px-2 mb-1">
      <span className="text-xl">Shared categories</span>
    </div>
  ) : (
    <MyCategoriesHeader />
  );
}
