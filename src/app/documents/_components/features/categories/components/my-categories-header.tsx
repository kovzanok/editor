"use client";
import { Plus } from "lucide-react";
import React from "react";

import { Button } from "@/components/ui/button";
import { useCategoryCreate } from "@/shared/hooks";

export function MyCategoriesHeader() {
  const { handleCreate, isLoading } = useCategoryCreate();
  return (
    <div className="flex items-center justify-between px-2 mb-1">
      <span className="text-xl">My categories</span>
      <Button
        className="p-0 h-8 w-8"
        disabled={isLoading}
        onClick={handleCreate}
        variant={"outline"}
      >
        <Plus />
      </Button>
    </div>
  );
}
