import React from "react";

import { Doc } from "@/../convex/_generated/dataModel";

import { CategoriesListHeader } from "./components/categories-list-header";
import { CategoryItem } from "./components/category-item";
import { CategoriesListSkeleton } from "./components/skeleton";

type CategoriesListProps = {
  isShared?: boolean;
  categories: (Doc<"categories"> | null)[] | undefined;
  onItemClick?: () => void;
};

export function Categories({
  categories,
  isShared,
  onItemClick,
}: CategoriesListProps) {
  if (isShared && (!categories || categories?.length === 0)) {
    return null;
  }
  return (
    <div>
      <CategoriesListHeader isShared={isShared} />
      <ul className="flex flex-col gap-y-2">
        {!categories ? (
          <CategoriesListSkeleton />
        ) : (
          categories.map(
            category =>
              category && (
                <CategoryItem
                  key={category._id}
                  onItemClick={onItemClick}
                  {...category}
                  isMyCategory={!isShared}
                />
              ),
          )
        )}
        {categories?.length === 0 && (
          <div className="px-2 text-sm text-center text-gray-500">
            Empty here. Probably, you want to create new category?
          </div>
        )}
      </ul>
    </div>
  );
}
