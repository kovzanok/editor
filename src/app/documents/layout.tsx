"use client";

import { Authenticated, AuthLoading, useConvexAuth } from "convex/react";
import { redirect, RedirectType } from "next/navigation";
import { useEffect } from "react";

import { Spinner } from "@/components/ui/spinner";
import { CategoryContextProvider } from "@/shared/context";

import { Sidebar } from "./_components/widgets/sidebar";

export default function DocumentsLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  const { isAuthenticated, isLoading } = useConvexAuth();
  useEffect(() => {
    if (!isAuthenticated && !isLoading) {
      redirect("/", RedirectType.replace);
    }
  }, [isAuthenticated, isLoading]);

  return (
    <CategoryContextProvider>
      <main className="h-full">
        <Authenticated>
          <div className="flex h-full">
            <Sidebar />
            <div className="h-full overflow-y-auto select-none flex-1">
              {children}
            </div>
          </div>
        </Authenticated>
        <AuthLoading>
          <div className="h-screen w-screen flex justify-center items-center">
            <Spinner size={"lg"} />
          </div>
        </AuthLoading>
      </main>
    </CategoryContextProvider>
  );
}
