import React, { PropsWithChildren } from "react";
import {
  FieldValues,
  FormProvider,
  SubmitHandler,
  UseFormReturn,
} from "react-hook-form";

interface FormWithSubmit<T extends FieldValues> extends UseFormReturn<T> {
  onSubmit?: SubmitHandler<T>;
  onBlur?: never;
}

interface FormWithBlurSubmit<T extends FieldValues> extends UseFormReturn<T> {
  onBlur?: SubmitHandler<T>;
  onSubmit?: never;
}

type FormWrapperProps<T extends FieldValues> =
  | FormWithSubmit<T>
  | FormWithBlurSubmit<T>;

export function FormWrapper<T extends FieldValues>({
  children,
  onBlur,
  onSubmit,
  ...methods
}: PropsWithChildren<FormWrapperProps<T>>) {
  return (
    <FormProvider {...methods}>
      <form
        onBlur={onBlur && methods.handleSubmit(onBlur)}
        onSubmit={onSubmit && methods.handleSubmit(onSubmit)}
      >
        {children}
      </form>
    </FormProvider>
  );
}
