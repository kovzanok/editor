import React from "react";
import { FieldValues, useFormContext } from "react-hook-form";

import { CategoryInput, CategoryInputProps } from "@/components/ui/input";

type ReactHookFormCategoryInputProps = CategoryInputProps & {
  name: keyof FieldValues;
};

export function ReactHookFormCategoryInput({
  ...inputProps
}: ReactHookFormCategoryInputProps) {
  const { register } = useFormContext();
  return <CategoryInput {...inputProps} {...register(inputProps.name)} />;
}
