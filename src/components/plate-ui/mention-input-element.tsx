import { cn, withRef } from "@udecode/cn";
import { getHandler, PlateElement } from "@udecode/plate-common";
import React from "react";
import { useFocused, useSelected } from "slate-react";

export const MentionInputElement = withRef<
  typeof PlateElement,
  {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    onClick?: (mentionNode: any) => void;
  }
>(({ className, onClick, ...props }, ref) => {
  const { children, element } = props;

  const selected = useSelected();
  const focused = useFocused();

  return (
    <PlateElement
      asChild
      className={cn(
        "inline-block rounded-md bg-slate-100 px-1.5 py-0.5 align-baseline text-sm dark:bg-slate-800",
        selected && focused && "ring-2 ring-slate-950 dark:ring-slate-300",
        className,
      )}
      data-slate-value={element.value}
      onClick={getHandler(onClick, element)}
      ref={ref}
      {...props}
    >
      <span>{children}</span>
    </PlateElement>
  );
});
