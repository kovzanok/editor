"use client";

import { useUserById } from "@udecode/plate-comments";
import React from "react";

import {
  Avatar,
  AvatarFallback,
  AvatarImage,
} from "@/components/plate-ui/avatar";

export function CommentAvatar({ userId }: { userId: string | null }) {
  const user = useUserById(userId);
  if (!user) {
    return null;
  }

  return (
    <Avatar className="size-5">
      <AvatarImage alt={user.name} src={user.avatarUrl} />
      <AvatarFallback>{user.name?.[0]}</AvatarFallback>
    </Avatar>
  );
}
