"use client";

import { cn } from "@udecode/cn";
import {
  CommentProvider,
  CommentsPositioner,
  SCOPE_ACTIVE_COMMENT,
  useFloatingCommentsContentState,
  useFloatingCommentsState,
} from "@udecode/plate-comments";
import { PortalBody } from "@udecode/plate-common";
import React from "react";

import { ScrollArea, ScrollBar } from "../ui/scroll-area";
import { CommentCreateForm } from "./comment-create-form";
import { CommentItem } from "./comment-item";
import { CommentReplyItems } from "./comment-reply-items";
import { popoverVariants } from "./popover";

export type FloatingCommentsContentProps = {
  disableForm?: boolean;
};

export function CommentsPopoverContent(props: FloatingCommentsContentProps) {
  const { disableForm } = props;

  const { ref, activeCommentId, hasNoComment, myUserId } =
    useFloatingCommentsContentState();

  return (
    <CommentProvider
      id={activeCommentId}
      key={activeCommentId}
      scope={SCOPE_ACTIVE_COMMENT}
    >
      <div className={cn(popoverVariants(), "relative w-[310px]")} ref={ref}>
        <ScrollArea className="w-full h-[200px] pr-3">
          {!hasNoComment && (
            <>
              <CommentItem commentId={activeCommentId} key={activeCommentId} />

              <CommentReplyItems />
            </>
          )}
          <ScrollBar />
        </ScrollArea>
        {!!myUserId && !disableForm && <CommentCreateForm />}
      </div>
    </CommentProvider>
  );
}

export function CommentsPopover() {
  const { loaded, activeCommentId } = useFloatingCommentsState();

  if (!loaded || !activeCommentId) {
    return null;
  }

  return (
    <PortalBody>
      <CommentsPositioner className="absolute z-50 w-[418px] !left-1/2 -translate-x-1/3 md:translate-x-0 pb-4">
        <CommentsPopoverContent />
      </CommentsPositioner>
    </PortalBody>
  );
}
