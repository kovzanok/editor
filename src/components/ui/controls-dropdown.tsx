import { EllipsisVertical } from "lucide-react";
import React, { PropsWithChildren, useState } from "react";

import { cn } from "@/shared/lib";

import { Button } from "./button";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from "./dropdown-menu";

type ControlsDropdownProps = {
  onOpenChange?: (open: boolean) => void;
  open?: boolean;
  disabled?: boolean;
  triggerButton?: React.ReactNode;
};

export function ControlsDropdown({
  open,
  onOpenChange,
  children,
  triggerButton,
  disabled,
}: PropsWithChildren<ControlsDropdownProps>) {
  const [innerOpen, setInnerOpen] = useState(open);
  return (
    <DropdownMenu
      onOpenChange={onOpenChange ? onOpenChange : setInnerOpen}
      open={open !== undefined ? open : innerOpen}
    >
      <DropdownMenuTrigger
        asChild
        className="focus-visible:!outline-0 focus-visible:!ring-transparent focus-visible:!ring-offset-0"
        disabled={disabled}
      >
        {triggerButton || (
          <Button
            className={cn(
              "group-hover:visible lg:invisible md:visible items-center self-end justify-center p-1 md:w-6 md:h-6 w-8 h-8",
              open && "visible",
            )}
            variant={"outline"}
          >
            <EllipsisVertical size={14} />
          </Button>
        )}
      </DropdownMenuTrigger>
      <DropdownMenuContent className="flex flex-col gap-y-1">
        {children}
      </DropdownMenuContent>
    </DropdownMenu>
  );
}
