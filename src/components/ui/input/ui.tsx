import * as React from "react";

import { cn } from "@/shared/lib";

export interface CategoryInputProps
  extends React.InputHTMLAttributes<HTMLInputElement> {}

const CategoryInput = React.forwardRef<HTMLInputElement, CategoryInputProps>(
  ({ className, type, ...props }, ref) => {
    return (
      <input
        className={cn(
          "!outline-none disabled:hover:bg-transparent disabled:bg-transparent hover:bg-gray-100 focus:bg-gray-100 rounded",
          className,
        )}
        ref={ref}
        type={type}
        {...props}
      />
    );
  },
);
CategoryInput.displayName = "CategoryInput";

export { CategoryInput };
