import { useMutation } from "convex/react";
import {
  DefaultFunctionArgs,
  FunctionReference,
  OptionalRestArgs,
} from "convex/server";
import { MouseEventHandler } from "react";
import { toast } from "sonner";

export const useRemoveById = <T extends DefaultFunctionArgs>(
  apiRemoveEndpoint: FunctionReference<"mutation", "public", T, null>,
  args: OptionalRestArgs<FunctionReference<"mutation", "public", T, null>>,
  toastData: {
    loading?: string | React.ReactNode;
    success?: string | React.ReactNode;
    error?: string | React.ReactNode;
  },
  beforePromiseCallback?: (args?: unknown) => void,
  afterPromiseCallback?: (args?: unknown) => void,
): MouseEventHandler<HTMLButtonElement> => {
  const remove = useMutation(apiRemoveEndpoint);

  const handleRemove: MouseEventHandler<HTMLButtonElement> = e => {
    e.stopPropagation();
    if (beforePromiseCallback) {
      beforePromiseCallback();
    }
    const promise = remove(...args).then(
      () => afterPromiseCallback && afterPromiseCallback(),
    );
    toast.promise(promise, toastData);
  };

  return handleRemove;
};
