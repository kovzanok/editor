import { useMutation } from "convex/react";
import {
  DefaultFunctionArgs,
  FunctionReference,
  OptionalRestArgs,
} from "convex/server";
import { MouseEventHandler, useState } from "react";
import { toast } from "sonner";

export const useEntityCreate = <T extends DefaultFunctionArgs>(
  apiRemoveEndpoint: FunctionReference<"mutation", "public", T, null | string>,
  args: OptionalRestArgs<
    FunctionReference<"mutation", "public", T, null | string>
  >,
  toastData: {
    loading?: string | React.ReactNode;
    success?: string | React.ReactNode;
    error?: string | React.ReactNode;
  },
) => {
  const create = useMutation(apiRemoveEndpoint);
  const [isLoading, setIsLoading] = useState(false);

  const handleCreate: MouseEventHandler<HTMLButtonElement> = e => {
    e.stopPropagation();
    setIsLoading(true);
    const promise = create(...args).then(() => setIsLoading(false));
    toast.promise(promise, toastData);
  };

  return { handleCreate, isLoading };
};
