import { api } from "@/../convex/_generated/api";

import { useEntityCreate } from "./use-entity-create";

export const useCategoryCreate = () => {
  const { handleCreate, isLoading } = useEntityCreate(
    api.categories.create,
    [],
    {
      loading: "Creating new category...",
      error: "Error while creating category",
      success: "New category's created",
    },
  );

  return { handleCreate, isLoading };
};
