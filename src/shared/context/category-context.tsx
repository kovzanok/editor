import { createContext, Dispatch, SetStateAction, useState } from "react";

export type CategoryContextValue = {
  isSharedCategory: boolean;
  setIsSharedCategory: Dispatch<SetStateAction<boolean>>;
};

export const CategoryContext = createContext<CategoryContextValue | null>(null);

export const CategoryContextProvider = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  const [isSharedCategory, setIsSharedCategory] = useState(false);
  return (
    <CategoryContext.Provider value={{ isSharedCategory, setIsSharedCategory }}>
      {children}
    </CategoryContext.Provider>
  );
};
