import { useQuery } from "convex/react";
import { useContext, useEffect } from "react";

import { api } from "@/../convex/_generated/api";
import { Id } from "@/../convex/_generated/dataModel";

import { CategoryContext, CategoryContextValue } from "./category-context";

export const useCategoryContext = (categoryId: Id<"categories">) => {
  const data = useQuery(api.sharedCategories.checkShared, { categoryId });
  const { setIsSharedCategory } = useContext(
    CategoryContext,
  ) as CategoryContextValue;
  useEffect(() => {
    if (data) {
      setIsSharedCategory(data.shared);
      return;
    }
    setIsSharedCategory(false);
  }, [data]);
};
