import { mutation, query } from "./_generated/server";

export const store = mutation({
  handler: async ctx => {
    const identity = await ctx.auth.getUserIdentity();

    if (!identity) {
      throw new Error("Called storeUser without authentication present");
    }

    const user = await ctx.db
      .query("users")
      .withIndex("by_token", q =>
        q.eq("tokenIdentifier", identity.tokenIdentifier),
      )
      .unique();

    if (user) {
      if (user.fullName !== identity.name) {
        await ctx.db.patch(user._id, {
          fullName: identity.name,
        });
      }
      return user._id;
    }

    return await ctx.db.insert("users", {
      fullName: identity.name,
      email: identity.email!,
      nickName: identity.nickname!,
      tokenIdentifier: identity.tokenIdentifier,
      avatarUrl: identity.pictureUrl,
    });
  },
});

export const get = query({
  handler: async ctx => {
    return ctx.db.query("users").collect();
  },
});

export const getTokenIdentifier = query({
  handler: async ctx => {
    const identity = await ctx.auth.getUserIdentity();
    if (!identity) {
      throw new Error("No authorization");
    }
    return { tokenIdentifier: identity.tokenIdentifier };
  },
});
