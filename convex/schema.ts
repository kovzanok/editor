import { defineSchema, defineTable } from "convex/server";
import { v } from "convex/values";

export default defineSchema({
  categories: defineTable({
    tokenIdentifier: v.string(),
    title: v.string(),
    description: v.string(),
  }).index("by_user", ["tokenIdentifier"]),
  sharedCategories: defineTable({
    categoryId: v.id("categories"),
    addedUsersTokens: v.array(v.string()),
  })
    .index("by_category", ["categoryId"])
    .index("by_user", ["addedUsersTokens"]),
  space: defineTable({
    title: v.string(),
    categoryId: v.id("categories"),
  }).index("by_category", ["categoryId"]),
  page: defineTable({
    categoryId: v.id("categories"),
    spaceId: v.id("space"),
    title: v.string(),
    description: v.optional(v.string()),
    content: v.optional(v.string()),
    icon: v.optional(v.string()),
  }).index("by_space", ["spaceId"]),
  comments: defineTable({
    commentId: v.string(),
    parentId: v.optional(v.string()),
    tokenIdentifier: v.string(),
    pageId: v.id("page"),
    createdAt: v.number(),
    value: v.string(),
  }).index("by_page", ["pageId"]),
  users: defineTable({
    tokenIdentifier: v.string(),
    email: v.string(),
    fullName: v.optional(v.string()),
    nickName: v.string(),
    avatarUrl: v.optional(v.string()),
  }).index("by_token", ["tokenIdentifier"]),
});
