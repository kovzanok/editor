import { v } from "convex/values";

import { mutation, query } from "./_generated/server";
import { removeSpace } from "./space";

export const create = mutation({
  handler: async ctx => {
    const identity = await ctx.auth.getUserIdentity();

    if (!identity) {
      throw new Error("No authorization");
    }

    const categoryId = await ctx.db.insert("categories", {
      title: "Untitled",
      tokenIdentifier: identity.tokenIdentifier,
      description: "",
    });

    await ctx.db.insert("sharedCategories", {
      categoryId,
      addedUsersTokens: [],
    });
  },
});

export const update = mutation({
  args: {
    title: v.optional(v.string()),
    description: v.optional(v.string()),
    _id: v.id("categories"),
  },
  handler: async (ctx, { _id, description, title }) => {
    const identity = await ctx.auth.getUserIdentity();

    if (!identity) {
      throw new Error("No authorization");
    }

    const category = await ctx.db.get(_id);

    if (!category) {
      throw new Error("Not found");
    }

    if (category?.tokenIdentifier !== identity.tokenIdentifier) {
      throw new Error("No allowed");
    }
    const updatedCategory = ctx.db.patch(_id, {
      title,
      description,
    });

    return updatedCategory;
  },
});

export const getUserCategories = query({
  handler: async ctx => {
    const identity = await ctx.auth.getUserIdentity();

    if (!identity) {
      throw new Error("No authorization");
    }
    const tokenIdentifier = identity.tokenIdentifier;
    return ctx.db
      .query("categories")
      .withIndex("by_user", q => q.eq("tokenIdentifier", tokenIdentifier))
      .order("asc")
      .collect();
  },
});

export const getById = query({
  args: { _id: v.id("categories") },
  handler: async (ctx, { _id }) => {
    const identity = await ctx.auth.getUserIdentity();

    if (!identity) {
      throw new Error("No authorization");
    }

    const category = await ctx.db.get(_id);

    if (!category) {
      throw new Error("Not found");
    }

    if (category.tokenIdentifier === identity.tokenIdentifier) {
      return category;
    }

    const sharedCategories = await ctx.db.query("sharedCategories").collect();
    const isShared = Boolean(
      sharedCategories.find(
        c =>
          c.categoryId === _id &&
          c.addedUsersTokens.includes(identity.tokenIdentifier),
      ),
    );
    if (isShared) {
      return category;
    }
    throw new Error("Not allowed");
  },
});

export const remove = mutation({
  args: { _id: v.id("categories") },
  handler: async (ctx, { _id }) => {
    const identity = await ctx.auth.getUserIdentity();

    if (!identity) {
      throw new Error("No authorization");
    }
    const { tokenIdentifier } = identity;

    const category = await ctx.db.get(_id);
    if (!category) {
      throw new Error("Not found");
    } else if (category.tokenIdentifier !== tokenIdentifier) {
      throw new Error("Not allowed");
    }

    const [sharedCategory] = await ctx.db
      .query("sharedCategories")
      .withIndex("by_category", q => q.eq("categoryId", _id))
      .collect();

    const space = await ctx.db
      .query("space")
      .withIndex("by_category", q => q.eq("categoryId", _id))
      .collect();
    await Promise.all(
      space.map(s => removeSpace(ctx, { _id: s._id, categoryId: _id })),
    );
    await ctx.db.delete(_id);
    await ctx.db.delete(sharedCategory._id);
  },
});
