import { v } from "convex/values";

import { mutation, query } from "./_generated/server";

export const create = mutation({
  args: {
    commentId: v.string(),
    parentId: v.optional(v.string()),
    tokenIdentifier: v.string(),
    pageId: v.id("page"),
    createdAt: v.number(),
    value: v.string(),
  },
  handler: async (ctx, args) => {
    const identity = await ctx.auth.getUserIdentity();

    if (!identity) {
      throw new Error("No authorization");
    }

    ctx.db.insert("comments", args);
  },
});

export const get = query({
  args: { pageId: v.id("page") },
  handler: async (ctx, args) => {
    const identity = await ctx.auth.getUserIdentity();

    if (!identity) {
      throw new Error("No authorization");
    }
    return ctx.db
      .query("comments")
      .withIndex("by_page", q => q.eq("pageId", args.pageId))
      .collect();
  },
});

export const remove = mutation({
  args: { id: v.string() },
  handler: async (ctx, args) => {
    const identity = await ctx.auth.getUserIdentity();

    if (!identity) {
      throw new Error("No authorization");
    }
    const { tokenIdentifier } = identity;

    const [comment] = await ctx.db
      .query("comments")
      .filter(q => q.eq(q.field("commentId"), args.id))
      .take(1);
    if (!comment) {
      throw new Error("Not found");
    } else if (comment.tokenIdentifier !== tokenIdentifier) {
      throw new Error("Unauthorized");
    }
    await ctx.db.delete(comment._id);
  },
});

export const update = mutation({
  args: { id: v.string(), value: v.string() },
  handler: async (ctx, args) => {
    const identity = await ctx.auth.getUserIdentity();

    if (!identity) {
      throw new Error("No authorization");
    }
    const { tokenIdentifier } = identity;

    const [comment] = await ctx.db
      .query("comments")
      .filter(q => q.eq(q.field("commentId"), args.id))
      .take(1);
    if (!comment) {
      throw new Error("Not found");
    } else if (comment.tokenIdentifier !== tokenIdentifier) {
      throw new Error("Unauthorized");
    }

    ctx.db.patch(comment._id, { value: args.value });
  },
});
