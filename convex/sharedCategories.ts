import { v } from "convex/values";

import { mutation, query } from "./_generated/server";

export const getSharedCategories = query({
  async handler(ctx) {
    const identity = await ctx.auth.getUserIdentity();

    if (!identity) {
      throw new Error("No authorization");
    }

    const sharedCategories = await ctx.db.query("sharedCategories").collect();

    const sharedWithUser = sharedCategories.filter(({ addedUsersTokens }) =>
      addedUsersTokens.includes(identity.tokenIdentifier),
    );

    return Promise.all(
      sharedWithUser.map(({ categoryId }) => ctx.db.get(categoryId)),
    );
  },
});

export const checkShared = query({
  args: { categoryId: v.id("categories") },
  async handler(ctx, { categoryId }) {
    const identity = await ctx.auth.getUserIdentity();

    if (!identity) {
      throw new Error("No authorization");
    }
    const category = await ctx.db.get(categoryId);

    if (category?.tokenIdentifier === identity.tokenIdentifier) {
      return { shared: false };
    }
    const sharedCategories = await ctx.db.query("sharedCategories").collect();

    const shared = Boolean(
      sharedCategories.find(
        c =>
          c.categoryId === categoryId &&
          c.addedUsersTokens.includes(identity.tokenIdentifier),
      ),
    );

    return {
      shared,
    };
  },
});

export const addUsers = mutation({
  args: { categoryId: v.id("categories"), tokensArr: v.array(v.string()) },
  handler: async (ctx, { categoryId, tokensArr }) => {
    const identity = await ctx.auth.getUserIdentity();

    if (!identity) {
      throw new Error("No authorization");
    }
    const { tokenIdentifier } = identity;
    const category = await ctx.db.get(categoryId);

    if (!category) {
      throw new Error("Not found");
    }

    if (category.tokenIdentifier !== tokenIdentifier) {
      throw new Error("Not allowed");
    }

    const [sharedCategory] = await ctx.db
      .query("sharedCategories")
      .withIndex("by_category", q => q.eq("categoryId", categoryId))
      .collect();

    if (!sharedCategory) {
      throw new Error("Not found");
    }

    ctx.db.patch(sharedCategory._id, {
      addedUsersTokens: [...sharedCategory.addedUsersTokens, ...tokensArr],
    });
  },
});

export const getAddedUsers = query({
  args: { _id: v.id("categories") },
  handler: async (ctx, { _id }) => {
    const identity = await ctx.auth.getUserIdentity();

    if (!identity) {
      throw new Error("No authorization");
    }

    const [sharedCategory] = await ctx.db
      .query("sharedCategories")
      .withIndex("by_category", q => q.eq("categoryId", _id))
      .collect();

    if (!sharedCategory) {
      throw new Error("Not found");
    }

    const category = await ctx.db.get(_id);

    if (!category) {
      throw new Error("Not found");
    }

    const [categoryOwner] = await ctx.db
      .query("users")
      .withIndex("by_token", q =>
        q.eq("tokenIdentifier", category.tokenIdentifier),
      )
      .collect();
    return Promise.all([
      categoryOwner,
      ...sharedCategory.addedUsersTokens.map(token =>
        ctx.db
          .query("users")
          .withIndex("by_token", q => q.eq("tokenIdentifier", token))
          .collect()
          .then(a => a[0]),
      ),
    ]);
  },
});

export const removeUser = mutation({
  args: { categoryId: v.id("categories"), userToken: v.string() },
  handler: async (ctx, { categoryId, userToken }) => {
    const identity = await ctx.auth.getUserIdentity();

    if (!identity) {
      throw new Error("No authorization");
    }

    const [sharedCategory] = await ctx.db
      .query("sharedCategories")
      .withIndex("by_category", q => q.eq("categoryId", categoryId))
      .collect();

    if (!sharedCategory) {
      throw new Error("Not found");
    }

    ctx.db.patch(sharedCategory._id, {
      addedUsersTokens: [
        ...sharedCategory.addedUsersTokens.filter(t => t !== userToken),
      ],
    });
  },
});
