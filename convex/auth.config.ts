export default {
  providers: [
    {
      domain: "https://equipped-sole-30.clerk.accounts.dev",
      applicationID: "convex",
    },
  ],
};
