import {
  DocumentByInfo,
  FieldTypeFromFieldPath,
  GenericDataModel,
  GenericMutationCtx,
  NamedIndex,
  NamedTableInfo,
} from "convex/server";
import { v } from "convex/values";

import { Id } from "./_generated/dataModel";
import { mutation, query } from "./_generated/server";

export const create = mutation({
  args: { spaceId: v.id("space"), categoryId: v.id("categories") },
  handler: async (ctx, { spaceId, categoryId }) => {
    const identity = await ctx.auth.getUserIdentity();

    if (!identity) {
      throw new Error("No authorization");
    }
    const pageId = await ctx.db.insert("page", {
      title: "New page",
      spaceId,
      categoryId,
    });

    return pageId;
  },
});

export const get = query({
  args: { spaceId: v.id("space") },
  handler: async (ctx, { spaceId }) => {
    const identity = await ctx.auth.getUserIdentity();

    if (!identity) {
      throw new Error("No authorization");
    }

    const pages = await ctx.db
      .query("page")
      .withIndex("by_space", q => q.eq("spaceId", spaceId))
      .collect();

    return pages;
  },
});

export const removePage = async <T extends GenericDataModel>(
  ctx: GenericMutationCtx<T>,
  { _id }: { _id: Id<"page"> },
) => {
  const identity = await ctx.auth.getUserIdentity();

  if (!identity) {
    throw new Error("No authorization");
  }

  const comments = await ctx.db
    .query("comments")
    .withIndex("by_page", q =>
      q.eq(
        "pageId",
        _id as FieldTypeFromFieldPath<
          DocumentByInfo<NamedTableInfo<T, "comments">>,
          NamedIndex<NamedTableInfo<T, "comments">, "by_page">[0]
        >,
      ),
    )
    .collect();
  await Promise.all(
    comments.map(({ _id }) => ctx.db.delete(_id as Id<"comments">)),
  );
  await ctx.db.delete(_id);
};

export const remove = mutation({
  args: { _id: v.id("page"), spaceId: v.id("space") },
  handler: removePage,
});

export const getById = query({
  args: {
    _id: v.id("page"),
    spaceId: v.id("space"),
    categoryId: v.id("categories"),
  },
  handler: async (ctx, { _id, categoryId, spaceId }) => {
    const identity = await ctx.auth.getUserIdentity();

    if (!identity) {
      throw new Error("No authorization");
    }

    const page = await ctx.db.get(_id);

    if (!page) {
      throw new Error("Not found");
    }

    if (page.categoryId !== categoryId || page.spaceId !== spaceId) {
      throw new Error("Not allowed");
    }

    return page;
  },
});

export const update = mutation({
  args: {
    title: v.optional(v.string()),
    description: v.optional(v.string()),
    content: v.optional(v.string()),
    _id: v.id("page"),
  },
  handler: async (ctx, args) => {
    const identity = await ctx.auth.getUserIdentity();

    if (!identity) {
      throw new Error("No authorization");
    }

    const page = ctx.db.patch(args._id, {
      ...args,
    });

    return page;
  },
});
