import {
  DocumentByInfo,
  FieldTypeFromFieldPath,
  GenericDataModel,
  GenericMutationCtx,
  NamedIndex,
  NamedTableInfo,
} from "convex/server";
import { v } from "convex/values";

import { Doc, Id } from "./_generated/dataModel";
import { mutation, query } from "./_generated/server";
import { removePage } from "./page";

export const create = mutation({
  args: { categoryId: v.id("categories") },
  handler: async (ctx, { categoryId }) => {
    const identity = await ctx.auth.getUserIdentity();

    if (!identity) {
      throw new Error("No authorization");
    }

    const category = await ctx.db.get(categoryId);

    if (!category) {
      throw new Error("Not found");
    }

    if (category.tokenIdentifier !== identity.tokenIdentifier) {
      throw new Error("Not allowed");
    }

    const spaceId = await ctx.db.insert("space", {
      title: "Untitled",
      categoryId,
    });

    await ctx.db.insert("page", {
      title: "Page",
      spaceId,
      categoryId,
    });
  },
});

export const get = query({
  args: { categoryId: v.id("categories") },
  handler: async (ctx, { categoryId }) => {
    const identity = await ctx.auth.getUserIdentity();

    if (!identity) {
      throw new Error("No authorization");
    }

    const space = await ctx.db
      .query("space")
      .withIndex("by_category", q => q.eq("categoryId", categoryId))
      .collect();

    return space;
  },
});

export const getById = query({
  args: { _id: v.id("space"), categoryId: v.id("categories") },
  handler: async (ctx, { _id, categoryId }) => {
    const identity = await ctx.auth.getUserIdentity();

    if (!identity) {
      throw new Error("No authorization");
    }
    const space = await ctx.db.get(_id);

    if (!space) {
      throw new Error("Not found");
    }

    if (space.categoryId !== categoryId) {
      throw new Error("Not allowed");
    }

    return space;
  },
});

export const removeSpace = async <T extends GenericDataModel>(
  ctx: GenericMutationCtx<T>,
  { _id, categoryId }: { _id: Id<"space">; categoryId: Id<"categories"> },
) => {
  const identity = await ctx.auth.getUserIdentity();

  if (!identity) {
    throw new Error("No authorization");
  }
  const { tokenIdentifier } = identity;
  const category = (await ctx.db.get(categoryId)) as Doc<"categories"> | null;
  if (!category) {
    throw new Error("Not found");
  } else if (category.tokenIdentifier !== tokenIdentifier) {
    throw new Error("Not allowed");
  }

  const pages = await ctx.db
    .query("page")
    .withIndex("by_space", q =>
      q.eq(
        "spaceId",
        _id as FieldTypeFromFieldPath<
          DocumentByInfo<NamedTableInfo<T, "pages">>,
          NamedIndex<NamedTableInfo<T, "pages">, "by_space">[0]
        >,
      ),
    )
    .collect();

  await Promise.all(
    (pages as Doc<"page">[]).map(({ _id }) => removePage(ctx, { _id })),
  );
  await ctx.db.delete(_id);
};

export const remove = mutation({
  args: { _id: v.id("space"), categoryId: v.id("categories") },
  handler: removeSpace,
});

export const update = mutation({
  args: {
    title: v.optional(v.string()),
    _id: v.id("space"),
  },
  handler: async (ctx, args) => {
    const identity = await ctx.auth.getUserIdentity();

    if (!identity) {
      throw new Error("No authorization");
    }

    await ctx.db.patch(args._id, {
      ...args,
    });
  },
});
